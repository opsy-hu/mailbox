/*
 * @file   mailbox_userspace.c
 * @author brwnrclse (Barry Harris), Irie Grant, Sidney Borel Hall
 * @date   10 December 2016
 * @version 1.0
 * @brief  A Linux user space program that proviees an example of working with
 *               the mailbox driver.
 *
 *               @see mailbox.c for driver definition
 *               @see /dev/mailbox for device driver
*/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define BUFF_LEN 1024
static char receive[BUFF_LEN];
static clock_t begin;
static clock_t end;

int main() {
  clock_t prog_begin;
  clock_t prog_end;

  prog_begin = clock();

  int status, dev;
  char msg[BUFF_LEN];

  printf("----------------- Mailbox test code -----------------\n\n");
  printf("----------------- 00 - Load the device driver -----------------\n\n");

  /* Time opening the device */
  begin = clock();
  dev = open("/dev/mailbox_dev", O_RDWR);
  end = clock();

  if (dev < 0) {
    perror("Result: Device Failed to open!!");
    return errno;
  } else {
    printf("Result: Device Loaded\n");
    printf("CPU time: %f\n\n", (double) (end - begin) / CLOCKS_PER_SEC);
  }

  printf("Ready for next test? (Enter)");
  getchar();

  printf("----------------- 01 - Write Message to Device -----------------\n\n");
  printf("Enter a message to send to the kernel module => ");
  scanf("%[^\n]%*c", msg);
  printf("-------- Message: %s \n\n", msg);

  /* Time writing to the device */
  begin = clock();
  status = write(dev, msg, strlen(msg));
  end = clock();

  if (status < 0) {
    perror("Result: Failed to write to device!!");
    return errno;
  } else {
    printf("Result: Message written\n");
    printf("CPU time: %f\n\n", (double) (end - begin) / CLOCKS_PER_SEC);
  }

  printf("Ready for next test? (Enter)");
  getchar();

  printf("\n\n");
  printf("----------------- 02 - Reading Message from Device -----------------");
  printf("\n\n");

  /* Time reading from device */
  begin = clock();
  status = read(dev, receive, BUFF_LEN);
  end = clock();

  if (status < 0) {
    perror("Result: Failed to read the message from the device!!");
    return errno;
  } else {
    printf("Result: %s was read back from the device\n", receive);
    printf("CPU time: %f\n\n", (double) (end - begin) / CLOCKS_PER_SEC);
  }

  prog_end = clock();
  printf("Total CPU time = %f\n",
        (double) (prog_end - prog_begin) / CLOCKS_PER_SEC);
  return 0;
}
