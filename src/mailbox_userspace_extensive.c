/*
 * @file   mailbox_userspace.c
 * @author brwnrclse (Barry Harris), Irie Grant, Sidney Borel Hall
 * @date   10 December 2016
 * @version 1.0
 * @brief  A Linux user space program that proviees an example of working with
 *               the mailbox driver.
 *
 *               @see mailbox.c for driver definition
 *               @see /dev/mailbox for device driver
*/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define BUFF_LEN 256
static char receive[BUFF_LEN];
static clock_t begin;
static clock_t end;

int read_dev(int device);
int write_dev(int device);

int main() {
  clock_t prog_begin;
  clock_t prog_end;

  prog_begin = clock();

  int dev, choice;

  printf("----------------- Mailbox test code -----------------\n\n");
  printf("----------------- 00 - Load the device driver -----------------\n\n");

  /* Time opening the device */
  begin = clock();
  dev = open("/dev/mailbox_dev", O_RDWR);
  end = clock();

  if (dev < 0) {
    perror("Result: Device Failed to open!!");
    return errno;
  } else {
    printf("Result: Device Loaded\n");
    printf("CPU time: %f\n\n", (double) (end - begin) / CLOCKS_PER_SEC);
  }

  printf("Ready to begin test? (Enter)");
  getchar();

  do {
    int status;

    printf("What would you like to do? 0/1/* (Read/Write/Exit): ");
    scanf("%i", &choice);

    switch(choice) {
      case 0:
        status = read_dev(dev);

        if (status < 0) {
          printf("Ending test early due to read error...\n");
        }

        printf("Ready for next test? (Enter)");
        getchar();
        printf("\n\n");
        break;
      case 1:
        status = write_dev(dev);

        if (status < 0) {
          printf("Ending test early due to write error...\n");
        }

        printf("Ready for next test? (Enter)");
        getchar();
        printf("\n\n");
        break;
      default:
        choice = 2;
        break;
    }
  } while (choice != 2);

  prog_end = clock();
  printf("Total CPU time = %f\n",
        (double) (prog_end - prog_begin) / CLOCKS_PER_SEC);
  return 0;
}

/**
 * @brief      Read a message from the mailbox.
 *
 * @param[int]  device  Our mailbox
 *
 * @return     0 if succesful; errno otherwise
 */
int read_dev(int device) {
  printf("\n\n");
  printf("----------------- 02 - Reading Message from Device -----------------");
  printf("\n\n");

  /* Time reading from device */
  begin = clock();
  int status = read(device, receive, BUFF_LEN);
  end = clock();

  if (status < 0) {
    perror("Result: Failed to read the message from the device!!");
    return errno;
  } else {
    printf("Result: \n%s was read back from the device\n", receive);
    printf("CPU time: %f\n\n", (double) (end - begin) / CLOCKS_PER_SEC);
  }

  return 0;
}

/**
 * @brief      Write a message to the mailbox.
 *
 * @param[int]  device  Our mailbox
 *
 * @return     0 if successful; errno otherwise
 */
int write_dev(int device) {
  printf("----------------- 01 - Write Message to Device -----------------\n\n");
  printf("Enter a message to send to the kernel module => ");

  char msg[BUFF_LEN];

  scanf("\n%[^\n]%*c", msg);
  printf("-------- Message: %s \n\n", msg);

  /* Time writing to the device */
  begin = clock();
  int status = write(device, msg, strlen(msg));
  end = clock();

  if (status < 0) {
    perror("Result: Failed to write to device!!");
    return errno;
  } else {
    printf("Result: Message written\n");
    printf("CPU time: %f\n\n", (double) (end - begin) / CLOCKS_PER_SEC);
  }

  return 0;
}
