/**
 * @file    mailbox.c
 * @author  brwnrclse (Barry Harris) & Irie Grant II & Sidny Borell Hall
 * @date    04 Dec 2016
 * @version 1.0
 * @brief   A device driver for persistent pipes aka mailboxes
*/

/* Kernel Programming */
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>
#include <linux/spinlock.h>
#include <linux/kfifo.h>
#include <linux/wait.h>
#include <linux/sched.h>

/* Module Information */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("brwnrclse (Barry Harris) & Irie Grant II & Sidny Borell Hall");
MODULE_DESCRIPTION("Device driver for named pipes");
MODULE_VERSION("1.0");

/* Prototypes */

static int mailbox_init(void);
static void mailbox_exit(void);
static int mailbox_open(struct inode* inode, struct file* file);
static int mailbox_release(struct inode* inode, struct file* file);
static ssize_t mailbox_read(struct file * file, char * buf,
              size_t count, loff_t *ppos);
static ssize_t mailbox_write(struct file *file, const char __user *buf,
                size_t count, loff_t *ppos);

#define MAILBOX_MSG_FIFO_SIZE 1024
#define MAILBOX_MSG_FIFO_MAX  128
#define DEVICE_NAME "mailbox_dev"
#define CLASS_NAME "mailbox_class"

/* Global Variables */

static int mailbox_major_num;
static struct class* mailbox_class = NULL;
static struct device* mailbox_device = NULL;
static DECLARE_KFIFO(mailbox_msg_fifo, char, MAILBOX_MSG_FIFO_SIZE);
static DEFINE_SPINLOCK(read_lock);
static DEFINE_SPINLOCK(write_lock);
static DECLARE_WAIT_QUEUE_HEAD(emptyq);
static bool message_read;
static bool message_write;
/* A simple ref table for each message written to the fifo */
static unsigned int mailbox_msg_len[MAILBOX_MSG_FIFO_MAX];
/* Read & Write indices */
static int mailbox_msg_idx_rd, mailbox_msg_idx_wr;

static const struct file_operations mailbox_fops = {
    .owner      = THIS_MODULE,
    .open       = mailbox_open,
    .read       = mailbox_read,
    .write      = mailbox_write,
    .release    = mailbox_release
};

/**
 * @brief      Initializer function, bootstraps our device and says what's up
 *
 * @return    0 if successful
 */
static __init int mailbox_init(void) {
    printk(KERN_INFO "Mailbox Init\n");

    mailbox_major_num = register_chrdev(0, DEVICE_NAME, &mailbox_fops);
    if (mailbox_major_num < 0) {
        printk(KERN_ALERT "Unable to register a major number\n");
        return mailbox_major_num;
    }
    printk(KERN_INFO "Registered mailbox with major number %d\n", mailbox_major_num);

    mailbox_class = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(mailbox_class)) {
        unregister_chrdev(mailbox_major_num, DEVICE_NAME);
        printk(KERN_ALERT "Failed to register mailbox class\n");
        return PTR_ERR(mailbox_class);
    }
    printk(KERN_INFO "Registered Mailbox device class.\n");

    mailbox_device = device_create(mailbox_class, NULL, MKDEV(mailbox_major_num, 0),
        NULL, DEVICE_NAME);
    if (IS_ERR(mailbox_device)) {
        class_destroy(mailbox_class);
        unregister_chrdev(mailbox_major_num, DEVICE_NAME);
        printk(KERN_ALERT "Failed to create mailbox device\n");
        return PTR_ERR(mailbox_device);
    }
    printk(KERN_INFO "Created Mailbox device class.\n");

    /* Initialize the fifo */
    INIT_KFIFO(mailbox_msg_fifo);
    return 0;
}

/**
 * @brief      Exit function, says goodbye and destroys the device
 */
static void mailbox_exit(void) {
    device_destroy(mailbox_class, MKDEV(mailbox_major_num, 0));
    class_unregister(mailbox_class);
    class_destroy(mailbox_class);
    unregister_chrdev(mailbox_major_num, DEVICE_NAME);
    printk(KERN_INFO "Mailbox Exit\n");
}

/**
 * @brief      Open (bootstrap) operations for our device
 *
 * @param      inode  The inode
 * @param      filp   The filp
 *
 * @return     0
 */
static int mailbox_open(struct inode* inode, struct file* file) {
    // TODO: Use bools to prevent multiple writes
    printk(KERN_ERR "Opening Mailbox\n");
    message_read = false;
    message_write = false;
    return 0;
}

/**
 * @brief      Copy data from the fifo to the
 *                   user space
 *
 * @param      file    The file
 * @param[in]  __user  The user
 * @param[in]  count   The count
 * @param      ppos    The ppos
 *
 * @return     Amount of data read
 */
static ssize_t mailbox_read(struct file * file, char * buf,
			  size_t count, loff_t *ppos) {
    int retval;
    unsigned int copied;
    printk(KERN_INFO "Entering Read from Mailbox Function\n");

    /*
     * Check the read flag to see if the string has
     * been read and indicate there is no more data to be read.
     */
    if (message_read)
        return 0;

    spin_lock(&read_lock);

    while (kfifo_is_empty(&mailbox_msg_fifo)) { /* mailbox empty */
        spin_unlock(&read_lock);
        printk(KERN_ERR "No message in Mailbox, waiting...\n");
        if (wait_event_interruptible(emptyq, !(kfifo_is_empty(&mailbox_msg_fifo))))
            return -ERESTARTSYS;
        spin_lock(&read_lock);
    }

    /*
     * Besides copying the string to the user provided buffer,
     * this function also checks that the user has permission to
     * write to the buffer, that it is mapped, etc.
     */
    printk(KERN_INFO "Reading from Mailbox\n");
    retval = kfifo_to_user(&mailbox_msg_fifo, buf,
        mailbox_msg_len[mailbox_msg_idx_rd], &copied);

    spin_unlock(&read_lock);

    /* TODO: Handle partial reads (don't let happen)*/

    mailbox_msg_idx_rd = (mailbox_msg_idx_rd+1) % MAILBOX_MSG_FIFO_MAX;
    message_read = true;
    return retval ? retval : copied;
}

/**
 * @brief      Placing data into the read FIFO
 *
 * @param      dev    The development
 * @param      attr   The attribute
 * @param[in]  buf    The buffer
 * @param[in]  count  The count
 *
 * @return     Amount of data written
 */
static ssize_t mailbox_write(struct file *file, const char __user *buf,
                size_t count, loff_t *ppos) {
    /* TODO: Check memory limit */
    int ret;
    unsigned int copied;

    printk(KERN_INFO "Entering Write from Mailbox Function\n");

    if ((mailbox_msg_idx_wr+1)%MAILBOX_MSG_FIFO_MAX == mailbox_msg_idx_rd) {
        printk(KERN_ERR "message length table is full\n");
        return -ENOSPC;
    }

    spin_lock(&write_lock);

    ret = kfifo_from_user(&mailbox_msg_fifo, buf, count, &copied);
    printk(KERN_INFO "Write to Mailbox\n");
    spin_unlock(&write_lock);
    wake_up_interruptible(&emptyq);
    /* TODO: Handle partial writes (don't let happen)*/

    mailbox_msg_len[mailbox_msg_idx_wr] = copied;
    mailbox_msg_idx_wr = (mailbox_msg_idx_wr+1) % MAILBOX_MSG_FIFO_MAX;
    message_write = true;

    return ret ? ret : copied;
}

/**
 * @brief      Close operations for our device
 *
 * @param      inode  The inode
 * @param      filp   The filp
 *
 * @return     0
 */
static int mailbox_release(struct inode* inode, struct file* file) {
    printk(KERN_INFO "Closing Mailbox\n");
    /* TODO: Clear table indices? */
    // kfifo_reset(&mailbox_msg_fifo);
    // mailbox_msg_idx_rd = mailbox_msg_idx_wr = 0;

    return 0;
}

module_init(mailbox_init);
module_exit(mailbox_exit);
