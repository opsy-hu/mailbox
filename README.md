# mailbox

Pseudo-device drivers for mailbox objects in Linux Kernel 3.16.

Project Plan Doc:
https://docs.google.com/document/d/1x_j4NWE5vHdh_2_OrrEiKN0hKlaH2btbgmqQXkGfcHo/edit?usp=sharing

## Implementation
Check  `/src/mailbox.c`
A presentation of our development can be found in `/presentation`

## Running

To run use the provided make file in `/src`

### Build the LKM

```bash

make 

# or 

make mailbox

```

### Load / Unload the LKM

```bash
# After building the module
make load


# After the module is loaded
make unload

```

### Test
A simple user space file accessing the mailbox is provided.

```bash

# After building and loading the module
make test

```

#### More extensive testing
More extensive tests, for example multiple reads/writes can be shown as well

```bash
# After building and loading the module
make test_extensive

```
